# ISSISP2017

Repository for the talk on Fault Injection at the ISSISP2017 summer school

## Talk

Use remarkjs via the remark-minion theme for hugo.
Press C to clone a display; then press P to switch to presenter mode

## Code

Target a STM32VLDiscovery board.
Find details in [Code/Readme.md](./code/Readme.md).
