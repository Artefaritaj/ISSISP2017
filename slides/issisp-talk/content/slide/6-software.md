+++
contentType = "md"
weight = 6
+++
---
class: center, middle, inria

.rect-back[
.valign[
<img src="./img/software.png" height="128px" width="128px" />
]]

&nbsp;

# Using faults against any software
 where things start to get fun

---
class: inria, middle, center

# The attacker got the power

to nop any desired instruction

## What could go wrong?

---
class: inria, middle

# Implicit security model
### (application point of view)

- I am in control of **my** memory (with some exceptions for DMA).
- I trust the OS (the OS is all powerful wrt me).
- The CPU is faithful, my instructions will be executed as intended (static integrity == dynamic integrity).
- The compiler has correctly translated the programer's intention. (Well we are optimistic, arent we?)
- ...

---
class: inria, middle

# Implicit security model
### (application point of view)

- I am in control of **my** memory (with some exceptions for DMA).
- I trust the OS (the OS is all powerful wrt me).
- .underline[The CPU is faithful, my instructions will be executed as intended (static integrity == dynamic integrity).]

---
class: inria

# Buffer overflow, the beast comes back to life

```C
char text[128];
unsigned char key[16];
...
char big_buffer[256];

//consider strncpy OK for the sake of the demonstration
//buffer overflow mitigated with explicit destination size parameter
strncpy(text, big_buffer, 128);
```
=> (actual assembly in our test app)

``` ASM
mov	r2, r5
mov	r0, r6
ldr	r1, [pc, #24]	; (big_buffer address)
bl	8004770 <strncpy>
```

---
class: inria

# Buffer overflow, the beast comes back to life

```C
char text[128];
unsigned char key[16];
...
char big_buffer[256];

//consider strncpy OK for the sake of the demonstration
//buffer overflow mitigated with explicit destination size parameter
strncpy(text, big_buffer, 128);
```
=> (with a fault injection)

``` ASM
nop ; what is r2 value now? -> depends on previously executed instructions
mov	r0, r6
ldr	r1, [pc, #24]	; (big_buffer address)
bl	8004770 <strncpy>
```

---
class: inria, middle, center

# So we tried to do it experimentally

---
class: inria, middle, center

# So we tried to do it experimentally
and we did not achieve to nop the desired instruction (during first experimental campaign).

But we did better...


---
class: inria, middle, center

<img src="./img/BO.png" height="100%" width="100%" />

Buffer overflow is achieved by modification of the destination address!

(And we do not know how this worked...)

---
class: inria

# Buffer overflow, mitigation with a little bird

```C
char text[128];
char canary[8];
unsigned char key[16];
...
char big_buffer[256];

//consider strncpy OK for the sake of the demonstration
//buffer overflow mitigated with explicit destination size parameter
strncpy(text, big_buffer, 128);

if(canary != canary_correct) {//if canary not correct (pseudocode)
  kill();
}
```

Can we modify the key by targeting a piece of code not related to it, but handling data close in memory (and avoiding the canary)?

---
class: inria

# Buffer overflow, mitigation with a little bird

```C
char text[128];
char canary[8];
unsigned char key[16];
...
char big_buffer[256];

//consider strncpy OK for the sake of the demonstration
//buffer overflow mitigated with explicit destination size parameter
strncpy(text, big_buffer, 128);

if(canary != canary_correct) {//if canary not correct (pseudocode)
  kill();
}
```

Can we modify the key by targeting a piece of code not related to it, but handling data close in memory (and avoiding the canary)?

# YES!

But we hardly control the new key value.
We do not have a proper explanation for the detailed processus leading to this effect.

---
class: inria, middle

# Practical
Self-evaluate your previous design, is it secure?

How can you improve it?

---
class: inria, middle

```C
int array_compare(unsigned char* a, unsigned char* b, size_t size) {
  int i;
  for(i=0; i < size; i++) {
    if(a[i] != b[i]) {
      return 0;
    }}
  return 1;
}

void verify_pin(char* candidate_pin) {
  if(try_counter > 0) {
    int comparison = array_compare(candidate_pin, true_pin, PIN_SIZE);
    if(comparison == 1) {
      try_counter = MAX_TRY;
      auth = 1;
    } else {
      try_counter--;
      auth = 0;
    }}
  else {
    auth = 0;
  }
  if(try_counter == 0) {
    kill();
  }
}
```

---
class: inria, middle

# All your secrets belong to me...

 ... if you have not deleted my backdoor!

- Not properly hidden (maybe some of you spotted it),
- but invisible to static analysis, it is dead code.

---
class: inria

# How to activate the backdoor?

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

=>

```ASM
08000598 <blink_wait>:
push	{r7, lr}
sub	sp, #8
add	r7, sp, #0
ldr	r3, [pc, #44]	; (80005cc <blink_wait+0x34>)
...
adds	r7, #8
mov	sp, r7
pop	{r7, pc}
.word	0xe00be00c ; @80005cc, 0xe00be00c = 3758874636
```

---
class: inria

# How to activate the backdoor?

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;//go to backdoor
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

<img src="./img/trap.jpg" height="100%" width="100%" />

---
class: inria

# How to activate the backdoor?

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;//go to backdoor
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

=>

```ASM
08000598 <blink_wait>:
push	{r7, lr}
sub	sp, #8
add	r7, sp, #0
ldr	r3, [pc, #44]	; (80005cc <blink_wait+0x34>)
...
adds	r7, #8
mov	sp, r7
nop ; pop	{r7, pc}
b other_verif ;.word	(0xe00b)e00c
b other_verif ;.word	0xe00b(e00c)
```

Gotcha!
