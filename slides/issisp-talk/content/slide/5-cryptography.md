+++
contentType = "md"
weight = 5
+++
---
class: center, middle, inria

.rect-back[
.valign[
<img src="./img/encryption.png" height="128px" width="128px" />
]]


&nbsp;

# Using faults against cryptography

---
class: inria, middle, center

# 1 - "Bellcore" attack against RSA-CRT

Chinese Remainder Theorem could more judiciously be called **Sunzi Remainder theorem**.
*Or speak about the American Modular Multiplication or the Greek guy division...*

---
class: inria
## RSA
Setup:
```C
n = pq
φ(n) = (p-1)(q-1)
e | gcd(e, φ(n)) = 1 and e < φ(n)
d = e^-1 mod φ(n)

(n, e) is public, d is private
```
Sign:
```C
S = M^d mod n
```

Verify signature:
```C
M = S^e mod n
```


---
class: inria

## RSA-CRT
Setup:
```C
n = pq
φ(n) = (p-1)(q-1)
e | gcd(e, φ(n)) = 1 and e < φ(n)
d = e^-1 mod φ(n)


(n, e) is public, d is private
```
Sign:
```C
dp = d mod (p-1)
dq = d mod (q-1)
qinv = q mod p
S1 = M^dp mod p
S2 = M^dq mod q
h = qinv(S1-S2) mod p

S = S2 + hq
```

---
class: inria

## Fault on RSA-CRT
Setup:
```C
n = pq
φ(n) = (p-1)(q-1)
e | gcd(e, φ(n)) = 1 and e < φ(n)
d = e^-1 mod φ(n)


(n, e) is public, d is private
```
Sign:
```C
dp = d mod (p-1)
dq = d mod (q-1)
qinv = q mod p
S1' = ?????????
S2 = M^dq mod q
h' = qinv(S1'-S2) mod p

S' = S2+h'q
```

Exploit:
```C
gcd(S'-S,n) = gcd(h'q-hq, n) = gcd(q(h'-h), n) = q
```

---
class: inria, middle, center

# 2 - DFA on AES (Giraud's model)

---
class: inria, center

# AES

&nbsp;

.inria-back[
<img src="./img/AES.png" height="60%" width="60%" />
]


---
class: inria, center

# Single-bit fault

&nbsp;

.inria-back[
<img src="./img/AES_fault.png" height="60%" width="60%" />
]

---
class: inria

# Exploitation

```C
C = K10 xor SB(M9)
C' = K10 xor SB(M9') with M9' = M9 xor e

C xor C' = SB(M9) xor SB(M9 xor e), such that e = 2^i
```

Solve previous equation, yielding *2-14* possibilities for M9.

```C
K10 = C xor SB(M9)
```
So we have *2-14* key possibilities for each (C, C') pair.

**Repeat the operation, with a new fault.**

At the end (*2.4* executions expected in average), only one K10 can explain our observations: the correct one.

---
class: inria, center, middle

# 3 - Fault models

---
class: inria, middle, center

### A fault is just too many electrons in the wrong place.

---
class: inria

How we interpret it is in the eye of the observer.

Effect:
- bit flip,
- stuck at 0,
- stuck at 1

Scope:
- single-bit,
- single-byte,
- single-word
- random*

Instructions:
- nop
- data tempering
- random jump
- random opcode

Program:
- dataflow tempering,
- controlflow modification

...

---
class: inria
# Real-life software fault model

The usual fault model used in theoretical works is the NOP fault model.
But it is not what is observed in practice.

From .red[* ]:
## During fetch

- The next opcode is corrupted, may be anything...
- If the new opcode has no side effects, virtual NOP
- If not you have side effects (random branch, ...), and often crashes.

## During decode for load from flash

The loaded value is corrupted.

.footnote[.red[* ] "Electromagnetic Fault Injection: Towards a Fault Model on a 32-bit Microcontroller", Moro et al., FDTC2013]
