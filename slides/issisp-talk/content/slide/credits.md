+++
contentType = "md"
weight = 10000
+++
---
class: inria

# Conclusion

You cannot protect hardware with software only.

You should include protections at **all abstraction levels**.

You cannot write a *truly* secure Verify PIN on the proposed device.

## But

You can strenghten your system.
- minimizing attack surface <=> minimizing system interface **AND** complexity.
- harden software at **ALL** vulnerable points of the whole **system**.

Beware, the security of your system is only as good as the security of the weakest component.

---
class: inria, middle

# Conclusion continued

Fault attacks are back with a vengeance!

We need systematic tools and processes to evaluate and mitigate software vulnerability wrt faults. It cannot be the same tools as used for *safety* critical applications (not the same threat model).

We need hardened performance cores. Imagine a SoC with 3 "standard" cores and 1 "hardened" one. Any application could ask to be executed on this core for security related operations.

---
class: inria, middle, center
# The End


---
class:  inria

# Credit

## Images

- Icons from [flaticon](http://www.flaticon.com).
- NASA for the ISS
- Bugey nuclear reactors photo from [www.entrepriseetdecouverte.fr](https://www.entrepriseetdecouverte.fr)
