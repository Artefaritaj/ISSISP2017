+++
contentType = "md"
weight = 2
+++
---
class: center, middle, inria

# Content
<div class="wrapper">
<ul class="flex cards">
  <li><h2><img src="./img/history.png" height="64px"/></h2>
    <p>
    Where do fault attacks come from?
    </p></li>
  <li><h2><img src="./img/flash-black.png" height="64px"/></h2>
    <p>How to perform hardware fault injection?
    </p></li>
  <li><h2><img src="./img/encryption.png" height="64px"/></h2>
    <p>Fault injection against cryptography.
    </p></li>
  <li><h2><img src="./img/software.png" height="64px"/></h2>
    <p>Fault injection against software. Where we show that physical fault attacks can <i>create</i> vulnerabilities in a sound program.
    </p></li>
  <li><h2><img src="./img/secure-shield.png" height="64px"/></h2>
    <p>How can we protect ourselves? Defense strategies to protect (generic) code and cryptography.
    </p></li>
</ul>
</div>
