+++
contentType = "md"
weight = 4
+++
---
class: center, middle, inria

.rect-back[
.valign[
<img src="./img/flash-black.png" height="128px" width="128px" />
]]

&nbsp;

# Experimental side: how we .underline[**do**] a fault injection?

---
class: center, middle, inria

# 1 - First principles

---
class: center, middle, inria

# Meet a synchronous circuit

&nbsp;

.inria-back[
<img src="./img/circuit.png" height="95%" width="95%" />
]

---
class: center, middle, inria

# Register memorization

&nbsp;

.inria-back[
<img src="./img/wavedrom/reg_mem.svg" height="95%" width="95%" />
]

---
class: center, middle, inria

# Clock glitch

&nbsp;

.inria-back[
<img src="./img/wavedrom/reg_clk_glitch.svg" height="95%" width="95%" />
]

---
class: center, middle, inria

# Underpowering / Temperature increase

&nbsp;

.inria-back[
<img src="./img/wavedrom/reg_underpowering.svg" height="95%" width="95%" />
]

---
class: middle, inria

# Setup time violation

The small duration (< 1ns) before the clock rising edge where the D
signal must be constant is called the setup time.

It can be a source of .underline[**non determinism**] during fault injection if this constraint is not met.

---
class: middle, inria

# 2 - Electromagnetic (EM) fault injection

---
class: middle, inria, center

.inria-back[
<img src="./img/chip.png" height="80%" width="80%" />
]

Mondrian?

---
class: middle, inria, center

.inria-back[
<img src="./img/chip_probe.png" height="80%" width="80%" />
]

---
class: inria, middle

EM fault injection is similar to a localized glitch. Glitches can reach Ground, VCC, Clock, any bus...

Resolution depends on the EM probe used but its order of magnitude is .underline[**1mm**].

---
class: middle, inria

# 3 - Laser fault injection.red[* ]

&nbsp;

.center.inria-back[
<img src="./img/laser_mechanism.svg" height="50%" width="50%" />
]

&nbsp;

.footnote[.red[* ] Explanation from the PhD dissertation of C. Roscian (advised by J.M. Dutertre)]
---
class: middle, inria

# 4 - Our EM fault injection setup

---
class: inria, middle, center

.inria-back[
<img src="./img/faustine_zoom.jpg" height="95%" width="95%" />
]

---
class: inria, middle, center

.inria-back[
<img src="./img/faustine.jpg" height="95%" width="95%" />
]
