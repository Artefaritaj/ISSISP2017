+++
contentType = "md"
weight = 1
+++
class: center, middle, inria

.logo[
![INRIA logo](./img/inria-logo.png)
]

# Fault injection attacks

ISSISP 2017

Gif-sur-Yvette, France

July 21st, 2017

### .underline[**Ronan Lashermes**], Sébanjila Kevin Bukasa, Hélène Le Bouder and Jean-Louis Lanet
.pull-right[from the LHS lab/TAMIS @ INRIA-RBA]


---
class: inria, middle

# Practical
Design a simple algorithm to authenticate a user to a microcontroller (ARMv7-M) with a Personal Identification Number (PIN).

A stub is provided on the [git](https://gitlab.com/Artefaritaj/ISSISP2017).

You can do it with pencil and paper if prefered.

```shell
cd Documents/
git clone https://gitlab.com/Artefaritaj/ISSISP2017.git
```

---
class: inria, middle

```C
int array_compare(unsigned char* a, unsigned char* b, size_t size) {
  int i;
  for(i=0; i < size; i++) {
    if(a[i] != b[i]) {
      return 0;
    }}
  return 1;
}

void verify_pin(char* candidate_pin) {
  if(try_counter > 0) {
    int comparison = array_compare(candidate_pin, true_pin, PIN_SIZE);
    if(comparison == 1) {
      try_counter = MAX_TRY;
      auth = 1;
    } else {
      try_counter--;
      auth = 0;
    }}
  else {
    auth = 0;
  }
  if(try_counter == 0) {
    kill();
  }
}
```

---
class: inria

.left-column[
### What?
]

.right-column[
Fault injection attacks are the exploitation of a provoked incorrect behaviour of the targeted device.
]

---
class: inria
count: false

.left-column[
### What?
### How?
]

.right-column[
.dim[Fault injection attacks are the exploitation of a provoked incorrect behaviour of the target.]

Any means necessary. Here we will focus on hardware fault injection.
]

---
class: inria
count: false

.left-column[
### What?
### How?
### For?
]

.right-column[
.dim[Fault injection attacks are the exploitation of a provoked incorrect behaviour of the target.]

.dim[Any means necessary. Here we will focus on hardware fault injection.]

World domination of course!

(we will start by recovering encryption key and create vulnerabilities in software)
]

---
class: inria
count: false

.left-column[
### What?
### How?
### For?
### Why?
]

.right-column[
.dim[Fault injection attacks are the exploitation of a provoked incorrect behaviour of the target.]

.dim[Any means necessary. Here we will focus on hardware fault injection.]

.dim[World domination of course!

(we will gently start by recovering encryption key and create vulnerabilities in software)]

To show mathematicians that physics always win. And to illustrate the risks of abstraction barriers wrt. information security.
]

---
class: inria, middle

# The attacker must have physical access to the system.

- Smartcard,
- IoT,
- Phones,
- Smart Keys (cars, hotels, ...),
- ...
