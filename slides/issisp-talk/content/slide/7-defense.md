+++
contentType = "md"
weight = 7
+++
---
class: center, middle, inria

.rect-back[
.valign[
<img src="./img/secure-shield.png" height="128px" width="128px" />
]]

&nbsp;

# How to protect ourselves

---
class: inria, middle, center

## 1 - Detect the "too many electrons in the wrong place"



---
class: inria, middle, center
count: false

## .dim[1 - Detect the "too many electrons in the wrong place"]
## 2 - Adapt the program to counter a fault model

---
class: inria, middle, center
count: false

## .dim[1 - Detect the "too many electrons in the wrong place"]
## .dim[2 - Adapt the program to counter a fault model]
## 3 - Enforce system and control flow integrity at all time

---
class: inria, middle, center

# 1 - Detecting and dealing with fault injection

---
class: inria, center

## Physically place sensors on the chip to detect abnormal behavior.

&nbsp;

.inria-back[
<img src="./img/guard.svg" height="90%" width="90%" />
]

&nbsp;

Guard signal must be 1 only when rising edge authorized.

---
class: inria, center, middle

.inria-back[
<img src="./img/wavedrom/guard.svg" height="95%" width="95%" />
]

Guard signal is often generated from the clock, by adding a delay.

Rising edge authorized only after critical path delay.
---
class: inria, center, middle
From the PhD of Loic Zussa

.inria-back[
<img src="./img/alarm_small.png" height="90%" width="90%" />
]
100V

---
class: inria, center, middle

.inria-back[
<img src="./img/alarm_big.png" height="90%" width="90%" />
]
200V


---
class: inria, center

## Add redundancy

&nbsp;

.inria-back[
<img src="./img/red_dup.svg" height="60%" width="60%" />
]

---
class: inria, center, middle

.inria-back[
<img src="./img/red_reciprocal.svg" height="60%" width="60%" />
]

---
class: inria, center, middle

.inria-back[
<img src="./img/red_codes.svg" height="60%" width="60%" />
]

---
class: inria, center

## Shielding

&nbsp;

.white-back[
<img src="./img/cpu.png" height="60%" width="60%" />
]

---
class: inria, center, middle

.white-back[
<img src="./img/cpu_shielded.png" height="60%" width="60%" />
]

Better if data in transit in shield wires. Check data at the end to ensure shield integrity.

---
class: inria, middle

# 2 - Countering fault models

Assuming a **one** nop fault model.
You *just* have to ensure that no one nop can induce a vulnerability.

The first step is to **prove** (formal method) that your program is not vulnerable if it misses one instruction.
With this tool you can manually design a code that should be secure against the one-nop fault model.

But here the compiler is not your friend. Some of it can be automatized...

---
class: inria
From the PhD of Nicolas Moro:

## Idempotent instructions

Same system state if instruction is repeated:

```ASM
mov r1,r8
mov r1,r8
add r3,r1,r2
add r3,r1,r2
```
You can remove any instruction, the program still operates as expected.

## Separable instructions

There exists a mapping between an instruction and a **safer** sequence of instruction:
```ASM
add r1, r1, r3
```
becomes (rx is available register)
```ASM
mov rx, r1
mov rx, r1
add r1, rx, r3
add r1, rx, r3
```
---
class: inria
Stack operations:
```ASM
stmdb sp!, { r1, r2, r3, lr }
```
```ASM
stmdb sp, { r1, r2, r3, lr }
stmdb sp, { r1, r2, r3, lr }
sub rx, sp, #16
sub rx, sp, #16
mov sp, rx
mov sp, rx
```

Branch with return (routine must be idempotent):
```ASM
bl routine
```
```ASM
adr r12, return_label
adr r12, return_label
add lr, r12, #1 ; lr=r12+1 (thumb mode)
add lr, r12, #1 ;
b routine
b routine
return_label:
```

Some instructions cannot be replaced by a safer sequence.
The overhead is quite large!

---
class: inria

# The problem with countering fault models

What is the correct fault model?

It is computationally expensive to prove safety. So we focus on sensitive code only. But...


---
class: inria

# 3 - Control Flow Integrity.red[ *]

Hardware solution for an hardware problem.

Encrypt your instructions, with program state encoding:
```C
i' = Ek(PC_prev || PC) xor i
```

```ASM
1: i1'
2: i2'
3: i3'
4: i4'
```
To decrypt i4:
```C
i4 = Ek(3||4) xor i4'
```
Nice if all instructions have only 1 predecessor...
If not we have a special case to deal with.


.footnote[.red[ *]Solution from: "SOFIA: Software and control flow integrity architecture", de Clercq et al., 2016]
