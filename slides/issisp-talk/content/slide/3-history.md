+++
contentType = "md"
weight = 3
+++
---
class: center, middle, inria

.rect-back[
.valign[
<img src="./img/history.png" height="128px" width="128px" />
]]

&nbsp;

# A bit of history

---

class: center, middle, inria

<img src="./img/iss.jpg" height="95%" width="95%" />

Faults before information security was a thing

---

class: center, middle, inria

<img src="./img/nuclear.jpg" height="95%" width="95%" />

Faults before information security was a thing

---
class: inria

## Radiations

Radiations were a threat to electronic devices from the start, sparking research on hardening chips in the 60s and 70s.

---
class: inria
count: false

## Radiations

.dim[Radiations were a threat to electronic devices from the start, sparking research on hardening chips in the 60s and 70s.]

A convenient way was invented to safely test designs: simulate faults with .underline[**lasers**].red[* ].

.footnote[.red[* ] "The Use of Lasers to Simulate Radiation-Induced Transients in Semiconductor Devices and Circuits", D. H. Habing, 1965]

---
class: inria
count: false

## Radiations

.dim[Radiations were a threat to electronic devices from the start, sparking research on hardening chips in the 60s and 70s.

A convenient way was invented to safely test designs: simulate faults with .underline[**lasers**].red[* ].]

## Cryptography

Then modern cryptography was invented: DES (1975) and RSA (1977).

.footnote[.dim[.red[* ] "The Use of Lasers to Simulate Radiation-Induced Transients in Semiconductor Devices and Circuits", D. H. Habing, 1965]]

---
class: inria
count: false

## Radiations

.dim[Radiations were a threat to electronic devices from the start, sparking research on hardening chips in the 60s and 70s.

A convenient way was invented to safely test designs: simulate faults with .underline[**lasers**].red[* ].]

## Cryptography

.dim[Then modern cryptography was invented: DES (1975) and RSA (1977).]

First (public) fault attacks against cryptography:
- the "Bellcore" attack against RSA .red[*2]
- Differential Fault Analysis .red[*3]


.footnote[.dim[.red[*] "The Use of Lasers to Simulate Radiation-Induced Transients in Semiconductor Devices and Circuits", Habing, 1965]

.red[*2] "On the importance of checking cryptographic protocols for faults", Boneh, DeMillo, Lipton, 1997

.red[*3] "Differential fault analysis of secret key cryptosystems", Biham, Shamir, 1997
  ]
