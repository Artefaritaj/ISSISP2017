# Preparation

The arm-none-eabi toolchain is required to build the project.
OpenOCD can be used to put the binary in the board.

Ubuntu:
```
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install openocd
```

# Build

Go in ISSISP2017/code/STM32/Firmware.

```
make
```

It's done.
