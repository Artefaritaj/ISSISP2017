
## Disassemble an elf

```bash
arm-none-eabi-objdump -S file.elf > file.elf.list
```

## openocd

Use script to launch server

```bash
./launch_ocd.sh
```

Connect with telnet:
```bash
telnet localhost 4444
```

Program (put an elf into a board):
```bash
program ../STM32/Firmware/firmware.elf
```

Some useful commands:
```bash
reset run
halt
```

Breakpoints
```bash
bp ADD 2
rbp ADD
```

Read in memory
```bash
stm32f1x.cpu mdw ADD WORD_COUNT
```
