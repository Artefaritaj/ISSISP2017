/**
 * @Author: Lashermes Ronan <ronan>
 * @Date:   12-06-2017
 * @Email:  ronan.lashermes@inria.fr
 * @Last modified by:   ronan
 * @Last modified time: 12-06-2017
 * @License: MIT
 */

#include "pin.h"
#include <string.h>
#include <stdio.h>

char my_secret[128] = "I have a Windows computer at home.\n\r";
int auth = 0;//1 if authenticated
unsigned char true_pin[PIN_SIZE] = {2, 1, 0, 7};
int try_counter = MAX_TRY;

int array_compare(unsigned char* a, unsigned char* b, size_t size) {
  int i;
  for(i=0; i < size; i++) {
    if(a[i] != b[i]) {
      return 0;
    }
  }
  return 1;
}

void verify_pin(char* candidate_pin) {
  if(try_counter > 0) {
    int comparison = array_compare(candidate_pin, true_pin, PIN_SIZE);

    if(comparison == 1) {
      try_counter = MAX_TRY;
      auth = 1;
    }
    else {
      try_counter--;
      auth = 0;
    }
  }
  else {
    auth = 0;
  }

  if(try_counter == 0) {
    kill();
  }
}

//call authenticate only if user is authenticated, recover the secret
void authenticated(char* secret, size_t max_size) {
  if(auth == 1) {
    strncpy(secret, my_secret, max_size);
  }
  else {
    sprintf(secret, "Operation not permitted.\n\r");
  }
}

//ask for an expensive action from the user before a new try (e.g. restart board...)
void mute() {
  int i;
  for(i=0; i < 5000000; i++);//wait a few seconds
}

//destroy the secret
void kill() {
  memset(my_secret, 0, sizeof(my_secret));
}
