# @Author: ronan
# @Date:   09-08-2016
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 19-09-2016
# @License: GPL



#IDIR is dir with includes
#ODIR is output dir with .o
#SDIR is dir with sources (.c, .s)
IDIR =./inc
ODIR =./obj
SDIR =./src

# Specify the compiler to use
CC=arm-none-eabi-gcc
# Specify the assembler to use
AS=arm-none-eabi-as
# Specity the linker to use
#LD=arm-none-eabi-ld
LD=arm-none-eabi-gcc

OBJDUMP = arm-none-eabi-objdump

#Dir to ST libs *********** TO CHANGE FOR YOUR MACHINE!!!!!!!!!!!
CUBE=../../common/STM32Cube_FW_F1_V1.3.0

#Various dirs for different kinds of libs
STMHAL_I=$(CUBE)/Drivers/STM32F1xx_HAL_Driver/Inc
STMHAL_C=$(CUBE)/Drivers/STM32F1xx_HAL_Driver/Src
CMSIS_I=$(CUBE)/Drivers/CMSIS/Include
DEVICE_I=$(CUBE)/Drivers/CMSIS/Device/ST/STM32F1xx/Include
BOARD=$(CUBE)/Drivers/BSP/STM32VL-Discovery

#Targeting STM32F1
TARGET = -mcpu=cortex-m3 -mthumb

#Compilation flags
CFLAGS += -I$(IDIR)
CFLAGS += -I$(STMHAL_I)
CFLAGS += -I$(CMSIS_I)
CFLAGS += -I$(DEVICE_I)
CFLAGS += -I$(BOARD)
CFLAGS += $(TARGET)
#Targeted device for ST libs
CFLAGS += -D STM32F100xB
#Debug symbols
#CFLAGS += -g
#Optimizations
CFLAGS += -O0

#Say where to look for some kinds of files
vpath %.c $(SDIR):$(STMHAL_C)
vpath %.s $(SDIR)

#Linker flags
LFLAGS = --specs=nosys.specs $(TARGET) -O3

#Dependencies
_DEPS = main.h stm32f1xx_hal_conf.h stm32f1xx_it.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

#Object files to link
_OBJ = main.o pin.o stm32f1xx_hal_msp.o stm32f1xx_it.o system_stm32f1xx.o startup_stm32f100xb.o stm32f1xx_hal.o stm32f1xx_hal_rcc.o stm32f1xx_hal_cortex.o stm32f1xx_hal_gpio.o stm32f1xx_hal_uart.o stm32f1xx_hal_dma.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

#C compilation rule
$(ODIR)/%.o: %.c $(DEPS)
	@echo CC $<
	@$(CC) -std=c99 -c -o $@ $< $(CFLAGS)

#Asssembly compilation rule
$(ODIR)/%.o: %.s $(DEPS)
	@echo AS $<
	@$(AS) $(ASFLAGS) $< -mcpu=cortex-m3 -mthumb -o $@

#Linker rule using specified linker script
firmware.elf: $(ODIR) $(OBJ)
	@echo Linking...
	@$(LD) $(OBJ) -T $(SDIR)/STM32F100VB_FLASH.ld $(LFLAGS) -o $@
	@$(OBJDUMP) -S $@ > $@.list
	@echo OK

$(ODIR):
	mkdir $@

#Cleaning
.PHONY: clean
clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~
	rm firmware.elf
