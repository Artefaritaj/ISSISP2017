/**
 * @Author: Lashermes Ronan <ronan>
 * @Date:   12-06-2017
 * @Email:  ronan.lashermes@inria.fr
 * @Last modified by:   ronan
 * @Last modified time: 12-06-2017
 * @License: MIT
 */



#ifndef PIN_H
#define PIN_H

#include <string.h>

#define MAX_TRY 3
#define PIN_SIZE 4

extern char my_secret[128];

//compare 2 arrays, 1 if identical
int array_compare(unsigned char* a, unsigned char* b, size_t size);

//compare candidate with true pin
void verify_pin(char* candidate_pin);

//call authenticate only if user is authenticated, recover the secret
void authenticated(char* secret, size_t max_size);

//ask for an expensive action from the user before a new try (e.g. restart board...)
void mute();

//destroy the secret
void kill();

#endif
